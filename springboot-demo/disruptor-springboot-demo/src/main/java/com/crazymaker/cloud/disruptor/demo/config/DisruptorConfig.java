package com.crazymaker.cloud.disruptor.demo.config;

import com.crazymaker.cloud.disruptor.demo.business.*;
import com.crazymaker.cloud.disruptor.demo.business.impl.DisruptorProducer;
import com.crazymaker.cloud.disruptor.demo.business.impl.ResizableDisruptorProducer;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class DisruptorConfig {


    @Autowired
    private MeterRegistry meterRegistry;


    @Bean
    public AsyncProducer asyncProducer() {
        //使用这一个简单 DisruptorProducer 生产者
        AsyncProducer asyncProducer=new DisruptorProducer();
//        AsyncProducer asyncProducer=new ResizableDisruptorProducer();
        asyncProducer.setMeterRegistry(meterRegistry);
        return asyncProducer;
    }
}